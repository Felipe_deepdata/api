<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('completions')) {
            return;
        }

        Schema::create('completions', function (Blueprint $table) {
            $table->increments('id_completions');
            $table->unsignedInteger('id_assignations')->nullable(false);
            $table->unsignedInteger('id_error_codes')->nullable(false);
            $table->decimal('extra_charges', 10, 2)->default(NULL);
            $table->string('extra_charges_reasons',2048)->default(NULL);
            $table->string('recipient', 128)->default(NULL);
            $table->decimal('tds_in', 10, 2)->default(NULL);
            $table->decimal('tds_out', 10, 2)->default(NULL);
            $table->string('used_parts', 512)->default(NULL);
            $table->char('latitude', 32)->default(NULL);
            $table->char('longitude', 32)->default(NULL);
            $table->string('work_description', 2048)->default(NULL);
            $table->string('serial_number', 128)->default(NULL);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('completions');
    }
}
