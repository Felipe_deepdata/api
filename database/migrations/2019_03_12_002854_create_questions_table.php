<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('questions')) {
            return;
        }

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id_questions');
            $table->unsignedInteger('id_polls')->nullable(false);
            $table->string('description', 256)->nullable(false);
            $table->enum('type', ['single', 'multiple']);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

        });
        Schema::table('options', function (Blueprint $table) {
            $table->foreign('id_questions', 'options_ibfk_1')
                ->references('id_questions')
                ->on('questions')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('options')) {
            Schema::table('options', function (Blueprint $table) {
                $table->dropForeign('options_ibfk_1');
            });
        }
        Schema::dropIfExists('questions');
    }
}
