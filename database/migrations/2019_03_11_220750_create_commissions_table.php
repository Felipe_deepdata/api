<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('commissions')) {
            return;
        }

        Schema::create('commissions', function (Blueprint $table) {
            $table->increments('id_commissions');
            $table->unsignedInteger('id_clients')->nullable(false);
            $table->decimal('amount', 10, 2)->nullable(false);
            $table->dateTime('calculated_at')->nullable(false);
            $table->dateTime('paid_at')->default(NULL);
            $table->boolean('is_active')->nullable(false);
            $table->integer('level_1')->nullable(false);
            $table->integer('level_2')->nullable(false);
            $table->integer('level_3')->nullable(false);
            $table->integer('level_4')->nullable(false);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
