@extends('layouts.base')

@section('title', 'Lista de Clientes con sus cargos y deudas')

@section('content')
    <div class="well">
        <h4>Lista de Clientes con: <strong class="text-warning">AguaGratis</strong><br><small>Y sus Referidos</small></h4>
        <hr>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Estatus de Cliente</th>
                <th>Menusalidad</th>
                <th>Deuda</th>
                <th>Pay Day</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{$client->id_clients}}</td>
                    <td>{{$client->name}}</td>
                    <td><strong>{{$client->status}}</strong></td>
                    <td>{{$client->monthly_fee / 100}}</td>
                    <td>{{number_format($client->debt / 100,2)}}</td>
                    <td>{{$client->pay_day}}</td>
                </tr>
                <tr>
                    <td colspan="7">
                        <div style="padding:0 20px;">
                            <div class="panel panel-default">
                                <div class="panel-heading" id="c_{{$client->id_clients}}">
                                    <h3 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#collapse{{$client->id_clients}}-debts" aria-expanded="true" aria-controls="collapse{{$client->id_clients}}-debts">
                                            Deudas (+)
                                        </a>
                                    </h3>
                                </div>
                                <div id="collapse{{$client->id_clients}}-debts" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$client->id_clients}}">
                                    <div class="panel-body">
                                        <h4>Deudas</h4>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-xs-3">ID</th>
                                                    <th class="col-xs-3">Monto</th>
                                                    <th class="col-xs-3">Id de Pago</th>
                                                    <th class="col-xs-3">Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($client->debts as $debt)
                                                    <tr>
                                                        <td>{{$debt->id_debts}}</td>
                                                        <td>{{($debt->amount + $debt->collection_fees + $debt->moratory_fees) / 100}}</td>
                                                        <td>{{$debt->id_charges}}</td>
                                                        <td>{{$debt->created_at}}</td>
                                                    </tr>
                                                    @if(count($debt->charge) > 0)
                                                    <tr>
                                                        <td colspan="4">
                                                            <table class="table" style="padding:20px; border: solid 2px #333;">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Descripcion</th>
                                                                        <th>Monto</th>
                                                                        <th>Pagado</th>
                                                                        <th>Fecha</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @foreach($debt->charge as $charge)
                                                                    <tr>
                                                                        <td>{{$charge->id_charges}}</td>
                                                                        <td>{{$charge->description}}</td>
                                                                        <td>{{$charge->amount / 100}}</td>
                                                                        <td>{{$charge->paid_at}}</td>
                                                                        <td>{{$charge->created_at}}</td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!--
                            <div class="panel panel-default">
                                <div class="panel-heading" id="c_{{$client->id_clients}}">
                                    <h3 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#collapse{{$client->id_clients}}-charges" aria-expanded="true" aria-controls="collapse{{$client->id_clients}}-charges">
                                            Cargos (+)
                                        </a>
                                    </h3>
                                </div>
                                <div id="collapse{{$client->id_clients}}-charges" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$client->id_clients}}">
                                    <div class="panel-body">
                                        <h4>Cargos</h4>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-xs-3">ID</th>
                                                    <th class="col-xs-3">Descripcion</th>
                                                    <th class="col-xs-3">Monto</th>
                                                    <th class="col-xs-3">Pagado</th>
                                                    <th class="col-xs-3">Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($client->charges as $charge)
                                                    <tr>
                                                        <td>{{$charge->id_charges}}</td>
                                                        <td>{{$charge->description}}</td>
                                                        <td>{{$charge->amount / 100}}</td>
                                                        <td>{{$charge->paid_at}}</td>
                                                        <td>{{$charge->created_at}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            -->
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

<script>
jQuery(function($){
    $('.openModal').on('click', function(e){
        e.preventDefault();
        var _this = $(this),
            _data = _this.data(),
            _monto = parseFloat(_data.info.amount+_data.info.collection_fees+_data.info.moratory_fees),
            _modal = {
                title: 'Cargo: '+_data.info.id_charges,
                message:'<h4>Deuda en ERP</h4>'+
                        '<table class="table">'+
                            '<tr><th>Deuda ID</th> <th>Monto</th> <th>Fecha</th></tr>'+
                            '<tr><td>'+_data.info.id_debts+'</td> <td>$ '+(_monto) / 100+'</td> <td>'+_data.info.created_at+'</td></tr>'+
                        '</table>',
                buttons: {
                    ok: {
                        label: "Cerrar",
                        className: 'btn-primary'
                    }
                }
            };

        bootbox.dialog(_modal);
    });
});
</script>
@endsection