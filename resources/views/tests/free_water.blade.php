@extends('layouts.base')

@section('title', 'Lista de Clientes con AguaGratis')

@section('content')
    <div class="well">
        <h4>Lista de Clientes con: <strong class="text-warning">AguaGratis</strong><br><small>Y sus Referidos</small></h4>
        <hr>
        <p>Los clientes que se muestran cumplen con los siguientes requisitos:</p>
        <ul>
            <li>Tiene al menos <strong>6 referidos</strong></li>
            <li>Los referidos están <strong>ACEPTADOS</strong></li>
        </ul>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Estatus de Cliente</th>
                <th>Estatus de Suscripcion</th>
                <th>CONEKTA</th>
                <th>Menusalidad</th>
                <th>Deuda</th>
                <th>Pay Day</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
                <tr>
                    <td>{{$client[id_clients]}}</td>
                    <td>{{$client[name]}}</td>
                    <td><strong>{{$client[status]}}</strong></td>
                    <td>{{$client[subscription_status]}}</td>
                    <td>{{$client[conekta_token]}}</td>
                    <td>{{$client[monthly_fee] / 100}}</td>
                    <td>{{number_format($client[debt] / 100,2)}}</td>
                    <td>{{$client[pay_day]}}</td>
                </tr>
                <tr>
                    <td colspan="7">
                        <div style="padding:0 20px;">
                            <div class="panel panel-default">
                                <div class="panel-heading" id="c_{{$client[id_clients]}}">
                                    <h3 class="panel-title">
                                        <a role="button" data-toggle="collapse" href="#collapse{{$client[id_clients]}}" aria-expanded="true" aria-controls="collapse{{$client[id_clients]}}">
                                            Referidos (+)
                                        </a>
                                        <span class="pull-right">{{count($client[treeValid])}} / {{count($client[tree])}}</span>
                                    </h3>
                                </div>
                                <div id="collapse{{$client[id_clients]}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$client[id_clients]}}">
                                    <div class="panel-body">
                                        <h4>Referidos Totales: <strong class="text-primary">{{count($client[tree])}}</strong><br>
                                        Referidos Válidos: <strong class="text-success">{{count($client[treeValid])}}</strong>
                                         </h4>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th class="col-xs-3">ID</th>
                                                    <th class="col-xs-3">Nombre</th>
                                                    <th class="col-xs-3">Estatus de Cliente</th>
                                                    <th class="col-xs-3">Estatus de Suscripcion</th>
                                                    <th class="col-xs-3">Menusalidad</th>
                                                    <th class="col-xs-3">Deuda</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($client[treeValid] as $referido)
                                                    <tr>
                                                        <td>{{$referido[id_clients]}}</td>
                                                        <td>{{$referido[name]}}</td>
                                                        <td><strong>{{$referido[status]}}</strong></td>
                                                        <td>{{$referido[subscription_status]}}</td>
                                                        <td>{{$referido[monthly_fee] / 100}}</td>
                                                        <td>{{number_format($referido[debt] / 100,2)}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

<script>
jQuery(function($){
    $('.openModal').on('click', function(e){
        e.preventDefault();
        var _this = $(this),
            _data = _this.data(),
            _monto = parseFloat(_data.info.amount+_data.info.collection_fees+_data.info.moratory_fees),
            _modal = {
                title: 'Cargo: '+_data.info.id_charges,
                message:'<h4>Deuda en ERP</h4>'+
                        '<table class="table">'+
                            '<tr><th>Deuda ID</th> <th>Monto</th> <th>Fecha</th></tr>'+
                            '<tr><td>'+_data.info.id_debts+'</td> <td>$ '+(_monto) / 100+'</td> <td>'+_data.info.created_at+'</td></tr>'+
                        '</table>',
                buttons: {
                    ok: {
                        label: "Cerrar",
                        className: 'btn-primary'
                    }
                }
            };

        bootbox.dialog(_modal);
    });
});
</script>
@endsection