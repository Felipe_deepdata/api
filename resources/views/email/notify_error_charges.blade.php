<div>
    <div style="text-align:center;">
        <img style="width:200px;" src="{{ asset('img/logo-aguagente-default.png') }}"/>
    </div>
    Hola {{ $name }}.
    <br/><br/>

    @if($pay_tries < 4)
        Su tarjeta ha sido rechazada, consulte con su banco para que podamos cobrar su pago.
        Intentaremos realizar el pago nuevamente
    @else
        Lamentablemente, aún no hemos podido cobrar su pago, según su contrato, la cláusula x, se cobrará un cargo por
        pago atrasado e intereses. Llámenos si necesita ayuda para realizar su pago antes de aplicar estas tarifas
    @endif
    <br/><br/>
    Gracias y disfruta de aguagente!
</div>