<div>
	<div style="text-align:center;">
		<img style="width:200px;" src="{{ asset('img/logo-aguagente-default.png') }}" />
	</div>
	Hola {{ $name }}, te enviamos tu factura correspondiente de {{$date}}.
	<br/><br/>
	{{$message_invoice}}
	<br/><br/>
	Gracias y disfruta de aguagente!
</div>