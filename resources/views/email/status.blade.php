<div>
	<div style="text-align:center;">
		<img style="width:200px;" src="{{ asset('img/logo-aguagente-default.png') }}" />
	</div>
	Hola {{ $name }}, te informamos que tu contrato ha sido {{ $status }}:
	<br/><br/>
	{{ $reasons }}
	<br/><br/>
	Gracias y disfruta de aguagente!
</div>