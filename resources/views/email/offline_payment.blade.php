El cliente <strong><?php echo $client->name;?></strong> ha realizado su pago.<br>
Fecha: <?php echo $charge->paid_at; ?><br>
Tipo: <?php echo strtoupper($data['type']);?><br>
Descripción: <?php echo $data['description'];?><br>
Conceptos:<br>
<?php foreach($data['line_items'] as $line) : ?>
    <?php echo $line['name'];?><br>
<?php endforeach; ?>
Total: $ <?php echo number_format($charge->amount/100,2);?> MXN
