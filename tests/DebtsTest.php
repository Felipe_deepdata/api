<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DebtsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testDebtIndex()
    {
        $this->get('/debts')
            ->assertResponseStatus(200);
    }

    public function testDebtShow()
    {
        $debt = $this->getObjectRandom(\App\Debt::class);
        $this->get('/debts/' . $debt->id_debts)
            ->assertResponseStatus(200);
    }

    public function testDebtCharge()
    {
        $debt = $this->getObjectRandom(\App\Debt::class);
        $this->post('/debts/' . $debt->id_debts . '/charge')
            ->assertResponseStatus(200);
    }

}
