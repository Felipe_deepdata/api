<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AssignationsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testAssignationIndex()
    {
        $this->get('/assignations')
            ->assertResponseStatus(200);
    }

    public function testAssignationShow()
    {
        $assignation = $this->getObjectRandom(\App\Assignation::class);
        $this->get('/assignations/' . $assignation->id_assignations)
            ->assertResponseStatus(200);
    }

}
