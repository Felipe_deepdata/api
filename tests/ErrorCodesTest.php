<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ErrorCodesTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Rol index test
     *
     * @return void
     */
    public function testErrorCodesIndex()
    {
        $this->get('/error-codes')
            ->assertResponseStatus(200);
    }

    public function testErrorCodesCreate()
    {
        $params = $this->getTestData('/data/error_codes/data.json');
        $this->post('/error-codes', $params)
            ->assertResponseStatus(201);
    }

    public function testErrorCodesUpdate()
    {
        $params = $this->getTestData('/data/error_codes/data.json');
        $errorCode = $this->getObjectRandom(\App\ErrorCode::class);
        $this->put('/error-codes/' . $errorCode->id_error_codes, $params)
            ->assertResponseStatus(200);
    }

    public function testErrorCodesDestroy()
    {
        $errorCode = $this->getObjectRandom(\App\ErrorCode::class);
        $this->delete('/error-codes/' . $errorCode->id_error_codes)
            ->assertResponseStatus(200);
    }

}
