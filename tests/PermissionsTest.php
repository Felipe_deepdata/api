<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PermissionsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * Permissions index test
     *
     * @return void
     */
    public function testPermissionsIndex()
    {
        $this->get('/permissions')
            ->assertResponseStatus(200);
    }

    public function testPermissionsCreate()
    {
        $params = $this->getTestData('/data/permissions/data.json');
        $this->post('/permissions', $params)
            ->assertResponseStatus(201);
    }

    public function testPermissionsUpdate()
    {
        $params = $this->getTestData('/data/permissions/data.json');
        $permission = $this->getObjectRandom(\App\Role::class);
        $this->put('/permissions/' . $permission->id, $params)
            ->assertResponseStatus(200);
    }

    public function testPermissionsDestroy()
    {
        $permission = $this->getObjectRandom(\App\Role::class);
        $this->delete('/permissions/' . $permission->id)
            ->assertResponseStatus(200);
    }

}
