<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ConfirmationsTest extends TestCase
{
    use WithoutMiddleware;
    use DatabaseTransactions;

    /**
     * index test
     *
     * @return void
     */
    public function testConfirmationIndex()
    {
        $this->get('/confirmations')
            ->assertResponseStatus(200);
    }

    public function testConfirmationCreate()
    {
        $ticket = $this->getObjectRandom(\App\Ticket::class);
        $params = [
            'id_tickets' => $ticket->id_tickets,
            'client_comments' => 'client comments'
        ];
        $this->post('/confirmations', $params)
            ->assertResponseStatus(200);
    }

    public function testConfirmationShow()
    {
        $confirmation = $this->getObjectRandom(\App\Confirmation::class);
        $this->get('/confirmations/' . $confirmation->id_confirmations)
            ->assertResponseStatus(200);
    }

}
