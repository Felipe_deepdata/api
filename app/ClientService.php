<?php namespace App;

use App\IClientRepository;

class ClientService {

	public function __construct(IClientRepository $clientRepository) {

		$this->clientRepository = $clientRepository;

	}

	/**
	 * isActive
	 * Devuelve "true" o "false", dependiendo si está activo.
	 * @require IClientRepository App\IClientRepository
	 * @param  integer  $id_clients
	 * @return boolean
	 */
	public function isActive($id_clients) {
		
		$amount = $this
			->clientRepository
			->getCommissionsFromPastMonth(
				$id_clients
			);

		if($amount >= 3000) {

			return true;

		} else if($amount > 1500) {

			return !empty($this
				->clientRepository
				->getSignedClientsFromThePastMonths($id_clients, 12));
			
		} else if($amount > 600) {

			return !empty($this
				->clientRepository
				->getSignedClientsFromThePastMonths($id_clients,  6));

		} else {

			return !empty($this
				->clientRepository
				->getSignedClientsFromThePastMonths($id_clients,  3));

		}

	}
	
}
