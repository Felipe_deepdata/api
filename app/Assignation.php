<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignation extends Model {

    protected $table = 'assignations';
    protected $primaryKey = 'id_assignations';

}
