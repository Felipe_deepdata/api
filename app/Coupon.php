<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
	protected $table = 'coupon';
	protected $primaryKey = 'id_coupon';

	public $rewards = [
		1  => '100',
		2  => '100',
		3  => '200',
		4  => '499',
		5  => '799',
		6  => '999',
		7  => '999',
		8  => '999',
		9  => '999',
		10 => '999',
		11 => '999',
		12 => '999',
		13 => '999',
		14 => '999',
		15 => '999',
		16 => '999',
		17 => '999',
		18 => '999',
		19 => '999',
		20 => '999'
	];
}
