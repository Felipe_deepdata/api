<?php
namespace App\Traits;

use App\Debt;
use App\Contract;
use App\Client;
use App\Charge;
use Request;
use Mail;
use App\Notification;

trait NotifyTrait
{
    /**
     * $staff
     *
     * @var array
     */
    protected $staff = [
        'servicio@aguagente.com',
        'raiders11@gmail.com'
        //'gus.knul@gmail.com'
    ];

    protected $admins = [
        'it@aguagente.com'
        //'gus.knul@gmail.com'
    ];

    protected $contacts = [
        'contacto@aguagente.com'
        //'gus.knul@gmail.com'
    ];

    protected $statusContract = "Estatus de contrato";
    protected $ticketCreation = "Creación de ticket # ";
    protected $ticketFinished = "Finalización de ticket # ";
    protected $ticketAssigned = "Asignación de ticket # ";

    /**
     * notifyUserOffline
     * Envía un email con la referencia generada para su pago OFFLINE.
     * 
     * @param string $type  Tipo de notificación(OXXO/SPEI)
     * @param object $order Objeto de CONEKTA con la respuesta del cargo.
     * @param Client $client
     * @return void
     */
    public function notifyUserOffline($type, $order, $client)
    {
        $view = 'email.oxxo';
        $data = [
            'reference' => $order->charges[0]->payment_method->reference,
            'total'     => $order->amount / 100
        ];

        if ($type == 'SPEI') {
            $view        = 'email.spei';
            $data['reference'] = $order->charges[0]->payment_method->clabe;
            $data['bank']      = $order->charges[0]->payment_method->bank;
        }

        $to = $this->staff;

        Mail::send($view, $data, function ($message) use ($client, $to) {
            $message->subject('Referencia de pago');
            //$message->to($client->email);
            $message->to($to);
            $message->replyTo('contacto@aguagente.com','Contacto');
        });
    }

    /**
     * notifyStaffOffilinePayment
     * Se le envía un email al staff cuando un pago offline se realice exitosamente. 
     * 
     * @param array $params Array de parametros
     * @return void
     */
    public function notifyStaffOffilinePayment($params)
    {
        $to = $this->staff;

        Mail::send('email.offline_payment', $params, function ($message) use ($to) {
            $message->subject('Pago offline exitoso');
            $message->replyTo('contacto@aguagente.com','Contacto');
            $message->to($to);
        });
    }

    /**
     * notifyPassword
     * Se le envía el email de bienvenida al cliente con su password.
     *
     * @param array $params Array de parametros
     * @return void
     */
    public function notifyPassword($params)
    {
        $staff = $this->staff;

        Mail::send(
            'email.passwordNotification',
            array(
                'name'     => $params['client']->name,
                'username' => $params['client']->email,
                'password' => $params['password']
            ),
            function ($message) use ($params) {
                $message
                    ->to($params['client']->email, $params['client']->name)
                    ->replyTo('contacto@aguagente.com','Contacto')
                    ->subject('Bienvenido a Aguagente');
            }
        );

        Mail::send(
            'email.staff_new_user',
            array(
                'client' => $params['client']
            ),
            function ($message) use ($params, $staff) {
                $message
                    ->to($staff)
                    ->replyTo('contacto@aguagente.com','Contacto')
                    ->subject('Nuevo registro');
            }
        );
    }

    public function notifyAdmin()
    {
        $admins = $this->admins;

        Mail::send(
            'email.staff_error',
            [],
            function ($message) use ($params, $admins) {
                $message
                    ->to($admins)
                    ->replyTo('contacto@aguagente.com','Contacto')
                    ->subject('Error procesando pago CRONJOB');
            }
        );
    }

    public function notifyStaff($client)
    {
        $data = array(
            'name' => 'Staff Aguagente',
            'msg' => 'Se actualizo el contrato de ' . $client->name
        );
        $this->sendEmail('email.generic', $data, $this->staff, 'Staff Aguagente', 'Contrato Actualizado');
    }

    public function notifyStatusContract($client, $status, $invalidation = null)
    {
        switch ($status) {
            case 'accepted':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Aceptado',
                    'reasons' => ''
                );
                break;

            case 'invalid':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Invalidado',
                    'reasons' => $invalidation->reasons
                );
                break;

            case 'rejected':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Rechazado',
                    'reasons' => $invalidation->reasons
                );
                break;

            case 'canceled':
                $data = array(
                    'name'    => $client->name,
                    'status'  => 'Cancelado',
                    'reasons' => $invalidation->reasons
                );
                break;
        }

        $this->sendEmail('email.status', $data, $client->email, $client->name, $this->statusContract, $client->id_users);
    }

    public function ticketCreation($ticket)
    {
        $data = array('ticket' => $ticket);
        $this->sendEmail(
            'ticket.creation',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketCreation . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }

    public function completionTicket($ticket, $completion, $technician)
    {
        $data = array(
            'technician' => $technician,
            'ticket'     => $ticket,
            'completion' => $completion
        );
        $this->sendEmail(
            'ticket.completion',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketFinished . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }

    public function assignedTicket($ticket, $assignation, $technician)
    {

        $data = array(
            'technician'  => $technician,
            'ticket'      => $ticket,
            'assignation' => $assignation
        );

        $this->sendEmail(
            'ticket.assignation',
            $data,
            $technician->email,
            $technician->name,
            $this->ticketAssigned . $ticket->id_tickets,
            $ticket->client->id_users
        );

        $data = array(
            'technician' => $technician,
            'ticket'     => $ticket
        );

        $this->sendEmail(
            'ticket.assignation-client',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketAssigned . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }


    public function endService($ticket, $completion, $technician)
    {
        $data = array(
            'technician' => $technician,
            'ticket'     => $ticket,
            'completion' => $completion
        );

        $this->sendEmail(
            'ticket.completion',
            $data,
            $ticket->client->email,
            $ticket->client->name,
            $this->ticketFinished . $ticket->id_tickets,
            $ticket->client->id_users
        );
    }

    public function contactEmail($client)
    {

        $data = array(
            'nombre'   => $client['nombre'],
            'correo'   => $client['correo'],
            'telefono' => $client['telefono'],
            'interesa' => $client['interesa'],
            'mensaje'  => $client['mensaje']
        );

        $this->sendEmail(
            'email.contact',
            $data,
            $this->contacts,
            'Contacto',
            'Contacto'
        );
    }

    public function sentPassword($input)
    {

        $data = array(
            'name'     => $input['name'],
            'username' => $input['email'],
            'password' => $input['password']
        );

        $this->sendEmail(
            'email.passwordNotification',
            $data,
            $input['email'],
            $input['name'],
            'Bienvenido a Aguagente'
        );
    }

    public function sendInvoiceToClient($invoice,$messageInvoice)
    {
        $name    = $invoice->client->name;
        $email   = $invoice->client->email;
        $subject = "Envio de Factura";
        $data    = array(
            'name'    => $name,
            'date'    => $invoice->date_stamp,
            'message_invoice' => $messageInvoice
        );
        $directory = public_path('invoices_files/');
        $pdfFile   = $directory . $invoice->filename . '.pdf';
        $xmlFile   = $directory . $invoice->filename . '.xml';
        Mail::send(
            'email.send_invoice',
            $data,
            function ($message) use ($email, $name, $subject, $pdfFile, $xmlFile) {

                $message->attach($pdfFile);
                $message->attach($xmlFile);

                $message->to(
                    $email,
                    $name
                )->subject($subject);
            }
        );
        Notification::create(array(
            'id_users' => $invoice->client->id_users,
            'message'  => $subject,
            'type'     => Notification::EMAIL
        ));
    }

    public function notifyClientErrorCharges($client)
    {
        $data = array(
            'nombre' => $client->name,
            'pay_tries' => $client->pay_tries
        );

        $this->sendEmail(
            'email.notify_error_charges',
            $data,
            $client->email,
            $client->name,
            'Error de cargo',
            $client->id_users
        );
    }

    public function notifyClientSuccessCharges($client)
    {
        $data = array(
            'nombre' => $client->name,
            'pay_tries' => $client->pay_tries
        );

        $this->sendEmail(
            'email.notify_charges',
            $data,
            $client->email,
            $client->name,
            'Error de cargo',
            $client->id_users
        );
    }

    private function sendEmail($view, $data, $email, $name, $subject, $id_users = null)
    {
        Mail::send(
            $view,
            $data,
            function ($message) use ($email, $name, $subject) {
                $message->to(
                    $email,
                    $name
                )->replyTo('contacto@aguagente.com','Contacto')
                    ->subject($subject);
            }
        );

        if ($id_users != null) {
            Notification::create(array(
                'id_users' => $id_users,
                'message'  => $subject,
                'type'     => Notification::EMAIL
            ));
        }
    }
}
