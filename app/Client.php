<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Client extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{

	use Authenticatable, Authorizable, CanResetPassword;

	protected $table = 'clients';
	protected $primaryKey = 'id_clients';
	protected $fillable = ['name', 'email', 'password'];
    protected $hidden = ['password', 'remember_token'];

    /**
     * Devuelve los campos almacenados en JSON en un ARRAY.
     *
     * @param  string  $value
     * @return array
     */
    public function getInvoiceDataAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Convierte el array en un texto JSON.
     *
     * @param  array  $value
     * @return void
     */
    /*public function setInvoiceDataAttribute($value)
    {
        $this->attributes['invoice_data'] = json_encode($value);
    }*/

    /**
     * getHierarchicalTree
     * Obtiene un array de los clientes, organizado en "ARBOL", por niveles o jerarquías.
     * @require MySQLClientRepository
     * @param  array $id_clients Array de id_clients
     * @return array             Array de clientes, organizado por niveles (App/Level)
     */
    public static function getHierarchicalTree($id_clients) {

        $clients = (new MySQLClientRepository())->getSignedClientsTree2($id_clients);
        $root = Client::find($id_clients)->toArray();

        switch($root['level']) {

            case Level::VIAJERO:
            case Level::MARINERO:
                $levels = [
                    'level_1' => array_column($clients, 'level_1')
                ];
                break;
            case Level::CABO:
                $levels = [
                    'level_1' => array_column($clients, 'level_1'),
                    'level_2' => array_column($clients, 'level_2')
                ];
                break;
            case Level::CAPITAN:
                $levels = [
                    'level_1' => array_column($clients, 'level_1'),
                    'level_2' => array_column($clients, 'level_2'),
                    'level_3' => array_column($clients, 'level_3')
                ];
                break;
            case Level::ALMIRANTE:
                $levels = [
                    'level_1' => array_column($clients, 'level_1'),
                    'level_2' => array_column($clients, 'level_2'),
                    'level_3' => array_column($clients, 'level_3'),
                    'level_4' => array_column($clients, 'level_4')
                ];
                break;

        }

        $tree[] = $root;

        foreach ($levels['level_1'] as $key => $client) {

            if( $client !== null && !isset($tree[0]['clients'][$client])) {

                $tree[0]
                    ['clients'][$client] = 
                        Client::find($client)->toArray();

            }

            if(isset($levels['level_2'])) {

                if($levels['level_2'][$key] !== null && !isset($tree[0]['clients'][$client]['clients'][$levels['level_2'][$key]])) {

                    $tree[0]
                        ['clients'][$client]
                            ['clients'][$levels['level_2'][$key]] 
                                = Client::find($levels['level_2'][$key])->toArray();


                
                }

            } 
            if(isset($levels['level_3'])) {

                if($levels['level_3'][$key] !== null && !isset($tree[0]['clients'][$client]['clients'][$levels['level_2'][$key]]['clients'][$levels['level_3'][$key]] )) {

                    $tree[0]
                        ['clients'][$client]
                            ['clients'][$levels['level_2'][$key]]
                                ['clients'][$levels['level_3'][$key]] 
                                    = Client::find($levels['level_3'][$key])->toArray();
                    
                }

            }
            if(isset($levels['level_4'])) {

                if($levels['level_4'][$key] !== null && !isset($tree[0]['clients'][$client]['clients'][$levels['level_2'][$key]]['clients'][$levels['level_3'][$key]]['clients'][$levels['level_4'][$key]] )) {

                    $tree[0]
                        ['clients'][$client]
                            ['clients'][$levels['level_2'][$key]]
                                ['clients'][$levels['level_3'][$key]]
                                    ['clients'][$levels['level_4'][$key]] 
                                        = Client::find($levels['level_4'][$key])->toArray();
                    
                }

            }

        }

        return $tree;

    }

    public function invoices()
    {
        return $this->hasMany('App\Invoice', 'id_clients');
    }
	
}
