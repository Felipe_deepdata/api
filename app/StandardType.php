<?php 

namespace App;

use InvalidArgumentException;
use ReflectionClass;

abstract class StandardType {

    /**
     * [$type description]
     * @var mixed
     */
    private $type;

    /**
     * __construct
     * @param mixed $type
     * @return void
     */
    public final function __construct($type) {
        
        $this->setType($type);

    }

    /**
     * setType
     * @param mixed $type
     * @return void
     */
    private function setType($type) {

        $class = get_called_class();
        $classConstans = (new ReflectionClass($class))->getConstants();

        if(!in_array($type, $classConstans))
            throw new InvalidArgumentException(
                lcfirst(end(explode('\\', $class))) . ' must be one of ' . implode(',', $classConstans)
            );
            
        $this->type = $type;

    }

    /**
     * getType
     * 
     * @return mixed
     */
    public final function getType() {

        return $this->type;

    }

}
