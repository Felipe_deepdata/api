<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionEvent extends Model {

	protected $table = 'subscription_events';
	protected $primaryKey = 'id_subscription_events';

}
