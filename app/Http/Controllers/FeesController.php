<?php

namespace App\Http\Controllers;

use App\Fee;

class FeesController extends Main 
{
    public function __construct()
    {
        parent::__construct(['index']);
    }

	/**
     * index
     * Devuelve todas los cargos (\App\Fee) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @App\Fee
     * 
     * @return response Ok
     */
    public function index() {

        foreach(Fee::get() as $fee) {

            $fees[$fee->brand][$fee->monthly_installments] = $fee->percentage;

        }

        return Main::response(true, 'OK', $fees, 200);

    }

}
