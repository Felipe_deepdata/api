<?php

namespace App\Http\Controllers;

use App\Device;
use App\Notification;
use Davibennun\LaravelPushNotification\PushNotification;
use Request;
use DB;
use Validator;

class PushController extends Main {

	/**
	 * push
	 * Envía una notificación "Push" a los dispositivos registrados.
	 *
	 * @Davibennun\LaravelPushNotification\PushNotification;
	 * @App\Notification;
	 * 
	 * @param  array $devices Objeto Eloquent de Dispositivos.
	 * @param  string $message Mensaje de notificacion
	 * @return mixed          void|ERROR
	 */
	public function push($devices, $message) {

		/*
		$push = new PushNotification();

		foreach($devices as $device) {

			$devices[$device->platform][] = $push->Device($device->registration_id, array('badge' => 1));
			$devices['users'][] = $device->id_users;

		}

		try {

			if(isset($devices['ios']))
				$push
					->app('aguagente-push-ios')
					->to($push->DeviceCollection($devices['ios']))
					->send($message);

			if(isset($devices['android']))
				$push
					->app('aguagente-push-android')
					->to($push->DeviceCollection($devices['android']))
					->send($message);

			foreach(array_unique($devices['users']) as $id_users) {

				$notification = new Notification;
				$notification->id_users = $id_users;
				$notification->message = $message;
				$notification->save();

			}

		} catch(\Exception $e) {

			file_put_contents('push', $e->getMessage());

		}
		*/

	}

	/**
	 * notifyUser
	 * Funcione principal que inicia el proceso para enviar notificaciones "PUSH" a clientes.
	 *  Obtiene el array de clientes y el mensaje por medio de la variable GET.
	 *
	 * @Illuminate\Foundation\Http\FormRequest
	 * 
	 * @return response OK|Bad Request
	 */
	public function notifyUser() {

		$input = Request::all();

		$rules = [
			'users'    => 'required|array',
			'message'  => 'required|string'
		];

		if(isset($input['users']))
			foreach($input['users'] as $id_users)
				$rules['users.0'] = 'exists:users,id';

		$validator = Validator::make($input, $rules);

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

		$this->push(
			DB::table('devices')->whereIn('id_users', $input['users'])->get(),
			$input['message']
		);

		return Main::response(true, 'OK', null, 200);

    }

    /**
     * notifyGroup
     * Funcion principal que inicia el proceso para enviar notificaciones "PUSH" a un grupo de clientes.
	 *  Obtiene el array de clientes y el mensaje por medio de la variable GET.
	 *
	 * @Illuminate\Foundation\Http\FormRequest
	 * 
	 * @return response OK|Bad Request
     */
	public function notifyGroup() {

		$input = Request::all();

		$rules = [
			'groups'   => 'required|array',
			'message'  => 'required|string'
		];

		if(isset($input['groups']))
			foreach($input['groups'] as $id_groups)
				$rules['groups.0'] = 'exists:groups,id_groups';

        $validator = Validator::make($input, $rules);

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

		$this->push(
			DB::table('devices')
				->join('users', 'users.id', '=', 'devices.id_users')
				->join('clients', 'users.id', '=', 'clients.id_users')
				->whereIn('clients.id_groups', $input['groups'])
				->select('devices.*')
				->get(),
			$input['message']
		);

		return Main::response(true, 'OK', null, 200);

	}

	/**
	 * notifyRole
	 * Funcion principal que inicia el proceso para enviar notificaciones "PUSH" a los usuarios que tengan cierto rol.
	 *  Obtiene el array de roles y el mensaje por medio de la variable GET.
	 *
	 * @Illuminate\Foundation\Http\FormRequest
	 * 
	 * @return response OK|Bad Request
	 */
	public function notifyRole() {

		$input = Request::all();

		$rules = [
			'roles'    => 'required|array',
			'message'  => 'required|string'
		];

		if(isset($input['roles']))
			foreach($input['roles'] as $id_roles)
				$rules['roles.0'] = 'exists:roles,id';

        $validator = Validator::make($input, $rules);

        if($validator->fails()) {

            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

        }

		$this->push(
			DB::table('devices')
				->join('role_user', 'role_user.user_id', '=', 'devices.id_users')
				->whereIn('role_user.role_id', $input['roles'])
				->select('devices.*')
				->get(),
			$input['message']
		);

		return Main::response(true, 'OK', null, 200);

	}

}
