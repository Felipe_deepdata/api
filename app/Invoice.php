<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    protected $table = 'invoices';
    protected $primaryKey = 'id_invoices';
    protected $fillable = ['id_clients', 'xml', 'date_stamp', 'filename'];


    public function client()
    {
        return $this->belongsTo('App\Client', 'id_clients');
    }

}