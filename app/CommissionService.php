<?php namespace App;

use App\Client;
use App\Level;
use Carbon\Carbon;

class CommissionService
{
	private function getClients($ids, $withDates = null)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $query = Client::whereIn('referred_by', $ids);
						//->whereRaw("((clients.level != 'VIAJERO') OR ((clients.subscription_status = 'active') && (clients.debt = 0)))")
						//->orderBy('created_at', 'DESC');
		
		if (!is_null($withDates)) {
			$query->where('collected_at', '<=', $withDates[0]);
		}
        
        return $query->get();
	}

	private function getLevelLimits($levelOrLoopNumber = null, $infoNeeded)
	{
		$limit = [
			'maxLoops'    => 1,
			'commission'  => 30,
			'diffInMonts' => 3
		];

		switch ($levelOrLoopNumber) {
			case 'CABO':
			case 2:
				$limit['maxLoops']    = 2;
				$limit['commission']  = 15;
				$limit['diffInMonts'] = 3;
				break;
			
			case 'CAPITAN':
			case 3:
				$limit['maxLoops']    = 3;
				$limit['commission']  = 10;
				$limit['diffInMonts'] = 6;
				break;
			
			case 'ALMIRANTE':
			case 4:
				$limit['maxLoops']    = 4;
				$limit['commission']  = 5;
				$limit['diffInMonts'] = 12;
				break;
		}

		return $limit[$infoNeeded];
	}
	
	/**
	 * calculateCommissions
	 * Función para calcular las comisiones de un cliente.
	 *   TODO
	 * 
	 * @param  Client $client [description]
	 * @return array          Array con los datos:
	 * 							TODO
	 */
	public function calculateCommissions(Client $client, $withDates = null, $withoutLimits = false)
	{
		$ids      = [];
        $idsDebug = [];
        $comision = [];
		$totals   = [];
		$declined = [];
		$return   = null;

		$clientLevel = $client->level;
		if ($withoutLimits) {
			$clientLevel = 'ALMIRANTE';
		}
        if ($clientLevel != 'VIAJERO') {
			
			$isActive = true;
			$maxLoops = $this->getLevelLimits($clientLevel, 'maxLoops');
            
            for ($i=1; $i <= $maxLoops; $i++) {

                $previousLevel   = 'level_'.($i-1);
				$levelCommission = $this->getLevelLimits($i, 'commission');
				$refids          = $ids[$previousLevel];
				$isLevel1        = ($i == 1);

				if ($isLevel1) {
					$refids = $client->id_clients;
				}

                if (isset($ids[$previousLevel]) || $isLevel1) {

					$referidos = $this->getClients($refids, $withDates);
					
					if ($isLevel1) {
						
						$refInverse         = array_reverse($referidos->toArray());
						$lastSignedUpClient = $refInverse[0];
						$today              = Carbon::now();
						$lastSUPCdate       = Carbon::parse($lastSignedUpClient['created_at']);
						$diff               = $today->diffInMonths($lastSUPCdate);
						$monthsForActive    = $this->getLevelLimits($clientLevel, 'diffInMonts');
						
						if (!is_null($withDates)) {
							$today = Carbon::parse($withDates[0]);
							$diff  = $today->diffInMonths($lastSUPCdate);
						}

						if ($diff > $monthsForActive) {
							$isActive = false;
						}
					}
					
					$freeWater = 0;
                    foreach ($referidos as $referido) {

                        $ids['level_'.$i][]      = $referido->id_clients;
                        $idsDebug['level_'.$i][] = [
                            'id'  => $referido->id_clients,
                            'ref' => $referido->referred_by
                        ];

                        if ($referido->status == 'accepted' && $referido->debt == '0.00') {
							if ($isLevel1 && $freeWater < 6) {
								$freeWater++;
							} else {
								$totals['level_'.$i]['id'][]       = $referido->id_clients;
								$totals['level_'.$i]['clients'][]  = $referido->toArray();
								$comision['level_'.$i]            += $levelCommission;
							}
                        } else {
                        	$declinedInfo = [
								'id'     => $referido->id_clients,
								'status' => $referido->status,
								'debt'   => $referido->debt
                        	];
                        	$declined['level_'.$i][$referido->id_clients] = $declinedInfo;
                        }
                    }
                }
			}

			foreach ($totals as $level => $value) {
				$count = count($totals[$level]['id']);

                $totals[$level]['count']     = $count;
				$totals[$level]['comission'] = $comision[$level];
				$totals[$level]['declined']  = $declined[$level];
			}
			$totals['isActive'] = $isActive;
			
			$return = $totals;
		}

		return $return;
	}

}