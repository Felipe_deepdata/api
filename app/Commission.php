<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model {

    protected $table = 'commissions';
    protected $primaryKey = 'id_commissions';

    /**
     * getDeclinedAttribute
     * Laravel Muttator GETTER, convierte el string en formato JSON almacenado, en un OBJETO de PHP
     *
     * @param array $value String en formato JSON de la base de datos.
     * @return object       Objeto JSON
     */
    public function getDeclinedAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * setDeclinedAttribute
     * Laravel Muttator SETER, convierte un array u objeto en JSON.
     *
     * @param array $value  Array de valores
     * @return void         
     */
    public function setDeclinedAttribute($value)
    {
        $this->attributes['declined'] = json_encode($value);
    }

}
