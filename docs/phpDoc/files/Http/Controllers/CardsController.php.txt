<?php

namespace App\Http\Controllers;

use App\Client;
use Conekta;
use Conekta_Customer;
use Request;
use Validator;

class CardsController extends Main {

	/**
     * create
     * Crea un una tarjeta directamente en CONEKTA, por medio del token de usuario asignado.
     * El cliente (\App\Client) realiza una búsqueda por ID.
     *
     * @\Conekta
     * @\App\Client
     * 
     * @param  int $id      ID de cliente
     * @return response     OK|Bad Request|Not Found
     */
    public function create($id) {

        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale("es");

        if($cliente = Client::find($id)) {
           
        	$input = Request::all();

        	$validator = Validator::make(
                $input,
                [
                    'token'  => 'required|string',
                ]
            );

            if($validator->fails()) {

                return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

            }

            $customer = Conekta_Customer::find($cliente->conekta_token);

            $card = $customer->createCard(array('token' => $input['token']));

            return Main::response(true, 'Created', json_decode($card->__toJSON()), 201);
            
        } else {

        	return Main::response(false, 'Not Found', null, 404);

        }
        
    }

    /**
     * index
     * Muestra todas las tarjetas guardads en CONEKTA, de un cliente.
     * El cliente (\App\Client), se busca mediante el id.
     * 
     * @\Conekta
     * @\App\Client
     * 
     * @param  int $id      ID de cliente
     * @return response     OK|Not Found
     */
    public function index($id) {

        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale("es");

        if($cliente = Client::find($id)) {

            $customer = Conekta_Customer::find($cliente->conekta_token);

            $cards = json_decode($customer->cards->__toJSON());

            foreach($cards as &$card) {

                $card->default =
                    (
                        isset($customer->default_card_id) && 
                              $customer->default_card_id == $card->id
                    ) ? true : false;

            }

            return Main::response(true, 'OK', $cards);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }
        
    }

	/**
     * show
     * Muestra la información de una tarjeta almacenada en CONEKTA.
     * Para obtener el $id_card, es necesario hacer una búsqueda del cliente(\App\Client) por ID.
     *
     * @\Conekta
     * @\Conekta_Customer
     * @\App\Client
     * 
     * @param  int      $id      ID de cliente
     * @param  string   $id_card ID de tarjeta
     * @return response          OK|Not Found
     */
    public function show($id, $id_card) {

        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale("es");

        if($cliente = Client::find($id)) {

        	$customer = Conekta_Customer::find($cliente->conekta_token);
        	
            $card = null;
            foreach($customer->cards as $c)
                if($c->id == $id_card)
                	$card = $c;
            
            if ($card) {

                $card = json_decode($card->__toJSON());

                $card->default =
                    (
                        isset($customer->default_card_id) && 
                              $customer->default_card_id == $card->id
                    ) ? true : false;

            	return Main::response(true, 'OK', $card);

            } else {

            	return Main::response(false, 'Not Found', null, 404);

            }

        } else {

        	return Main::response(false, 'Not Found', null, 404);

        }
        
    }

    /**
     * delete
     * Elimina una tarjeta asignada a un cliente en CONEKTA
     * Para obtener el $id_card, es necesario hacer una búsqueda del cliente(\App\Client) por ID.
     *
     * @\Conekta
     * @\Conekta_Customer
     * @\App\Client
     * 
     * @param  int       $id      ID de cliente
     * @param  string    $id_card ID de tarjeta
     * @return response            OK|Not Found
     */
    public function delete($id, $id_card) {

        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale("es");

        $input = Request::all();

        if($cliente = Client::find($id)) {

        	$customer = Conekta_Customer::find($cliente->conekta_token);
        	
            $card = null;
            foreach($customer->cards as $c)
                if($c->id == $id_card)
                	$card = $c;
            
            if ($card) {

            	$card->delete();

            	return Main::response(true, 'OK', null);

            } else {

            	return Main::response(false, 'Not Found', null, 404);

            }

        } else {

        	return Main::response(false, 'Not Found', null, 404);

        }
                        
    }

    /**
     * setAsDefault
     * Asigna una tarjeta como PREDETERMINADA para realizar los cobros en CONEKTA.
     * Para obtener el $id_card, es necesario hacer una búsqueda del cliente(\App\Client) por ID.
     *
     * @\Conekta
     * @\Conekta_Customer
     * @\App\Client
     * 
     * @param  int       $id      ID de cliente
     * @param  string    $id_card ID de tarjeta
     * @return response            OK|Not Found
     */
    public function setAsDefault($id, $id_card) {

        Conekta::setApiKey(env('CONEKTA_API_KEY', 'key_w6J8jsPrcy6byu2rX6WSYg'));
        Conekta::setLocale("es");

       	if($cliente = Client::find($id)) {

        	$customer = Conekta_Customer::find($cliente->conekta_token);

            $card = null;
            foreach($customer->cards as $c)
                if($c->id == $id_card)
                	$card = $c;
            
            if($card) {

                $customer->update(
                  array(
                    'default_card_id' => $id_card
                  )
                );

            	return Main::response(true, 'OK', json_decode($card->__toJSON()));

            } else {

            	return Main::response(false, 'Not Found', null, 404);

            }

        } else {

        	return Main::response(false, 'Not Found', null, 404);

        }

    }

}
