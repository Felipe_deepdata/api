<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\Main;
use DB;
use Request;

class CategoriesController extends Main {

    /**
     * __construct
     * Se le indica que la funcion "index" no debe usar AUTH, por medio del controlador MAIN
     */
    public function __construct() {

        parent::__construct(['index']);

    }

    /**
     * index
     * Devuelve todas las categorías (\App\Category) después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response NULL|Error(404)
     */
    public function index() {
        
        try {

            $categories = DB::table('categories');

            foreach(Request::query() as $name => $value) {
                
                $categories = $categories->where($name, $value);

            }

            $categories = $categories->where('active', 1)->get();

            foreach($categories as &$category) {

                $category = $this->resolveRelations($category);

            }

            return Main::response(true, null, $categories, 200, ['Cache-Control' => 'max-age=86400']);

        } catch(\Exception $e) {

            return Main::response(false, $e->getMessage(), null, 400);

        }
        
    }

    /**
     * create
     * Crea un una categoría.
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Category
     * 
     * @return response     NULL|ERROR(404)
     */
    public function create() {

        try {

            $input = Request::all();

            $category = new Category;
            $category->name = $input['name'];
            $category->description = $input['description'];
            $category->save();

            return Main::response(true, null, $this->resolveRelations($category), 201);

        } catch(\Exception $e) {

            return Main::response(false, null, null, 400);
            
        }        

    }

    /**
     * show
     * Muestra una categoría (\App\Category) por medio del ID
     *
     * @\App\Category
     * 
     * @param  int      $id ID de la categoría
     * @return response     NULL|ERROR(404)
     */
    public function show($id) {

        $category = Category::find($id);

        if($category && $category->active == 1) {

            return Main::response(true, null, $this->resolveRelations($category));

        } else {

            return Main::response(false, null, null, 404);

        }

    }

    /**
     * update
     * Guarda las modificaciones realizadas a una categoría (\App\Category) por medio del ID
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Category
     * 
     * @param  int      $id ID de la categoría
     * @return response     NULL|ERROR(404)
     */
    public function update($id) {

        $category = Category::find($id);
        
        if($category && $category->active == 1) {
            
            try {

                $input = Request::all();

            //  $category->id_categories = $input['id_categories'];
                $category->name = $input['name'];
                $category->description = $input['description'];
                $category->save();

                return Main::response(true, null, $this->resolveRelations($category));

            } catch(\Exception $e) {

                return Main::response(false, null, null, 400);
                
            }

        } else {

            return Main::response(false, null, null, 404);

        }

    }

    /**
     * destroy
     * Desactiva una categoría(\App\Category) por medio de un ID
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Category
     * 
     * @param  int      $id ID de la categoría
     * @return response     NULL|ERROR(404)
     */
    public function destroy($id) {

        $category = Category::find($id);
        
        if($category && $category->active == 1) {
            
            $category->active = 0;
            $category->save();

            return Main::response(true, null, null);

        } else {

            return Main::response(false, null, null, 404);

        }

    }

    /**
     * resolveRelations
     * Devuelve todos los categoriesElements que pertenescan a una categoría(\App\Category)
     * 
     * @param  Category $category Objeto Eloquent Category
     * @return Category           Objeto Eloquent Category
     */
    private function resolveRelations($category) {

        $elements = DB::table('categories_elements')
            ->where('id_categories', $category->id_categories)
            ->where('active', '=', 1)
            ->get();

        $category->elements = $elements;

        return $category;

    }

}

