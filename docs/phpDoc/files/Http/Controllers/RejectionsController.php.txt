<?php

namespace App\Http\Controllers;

use App\Rejection;
use App\Http\Controllers\Main;
use DB;
use Request;

class RejectionsController extends Main {

    /**
     * index
     * Un rechazo es ....
     * Devuelve todas los rechazos después de haber aplicado un filtros.
     * Los filtros se obtienen de la variable GET, pormedio del trait de Laravel REQUEST
     *
     * @Illuminate\Foundation\Http\FormRequest
     * 
     * @return response NULL|Internal Server Error
     */
    public function index() {
        
        try {

            $rejections = DB::table('rejections');

            foreach(Request::query() as $name => $value) {
                
                $rejections = $rejections->where($name, $value);

            }

            return Main::response(true, null, $rejections->get());

        } catch(\Exception $e) {

            return Main::response(false, 'Internal Server Error', null, 500);

        }
   
    }

    /**
     * show
     * Muestra un rechazo (\App\Rejection) por medio del ID
     *
     * @\App\Rejection
     * 
     * @param  int      $id ID del rechazo
     * @return response     OK|Not Found(404)
     */
    public function show($id) {

        if($rejection = Rejection::find($id)) {

            return Main::response(true, 'OK', $rejection);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

}
