<?php

namespace App\Http\Controllers;

use App\Poll;
use App\Answer;
use App\Question;
use App\Option;
use App\Client;
use App\Http\Controllers\Main;
use DB;
use Auth;
use Request;
use Validator;

class PollsController extends Main {

	/**
     * index
     * Devuelve todas las encuestas.
     * 
     * @return response Ok|Error(404)
     */
	public function index() {

		try {

			return Main::response(true, 'OK', DB::table('polls')->get());

		} catch(\Exception $e) {

			return Main::response(false, null, $e->getMessage(), 500);

		}
		
	}

	/**
     * show
     * Muestra una encuesta (\App\Poll) por medio del ID
     *
     * @\App\Poll
     * 
     * @param  int      $id ID de la encuesta
     * @return response     OK|Not Found(404)
     */
	public function show($id) {
		
		$poll = Poll::find($id);

        if ($poll) {

            return Main::response(true, 'OK', $this->resolveRelations($poll));

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }
        
	}

	/**
     * create
     * Crea un una encuesta, le asigna las preguntas y las opciones de respuesta a cada pregunta.
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Poll
     * @\App\Question
     * @\App\Option
     * 
     * @return response     Created|Bad Request|ERROR(404)
     */
	public function create() {

		try {

			$input = Request::all();

	    	$rules = [
                'name'           => 'required|string',
                'start_date'     => 'required|date',
                'end_date'       => 'required|date',
                'questions'      => 'required|array'
            ];

            if(isset($input['questions']) && 
            		 $input['questions']) {

		        foreach(range(0, count($input['questions']) - 1) as $i) {

	                $rules["questions.$i.description"]  = 'required|string';
	                $rules["questions.$i.type"]  		= 'required|string|in:single,multiple';
	                $rules["questions.$i.options"] 		= 'required|array';

	                if(isset($input['questions'][$i]['options']) && 
	                		 $input['questions'][$i]['options']) {

		                foreach(range(0, count($input['questions'][$i]['options']) - 1) as $k) {

			                $rules["questions.$i.options.$k.description"] = 'required|string';

			            }

	                }

	            }

            }
            	
            $validator = Validator::make($input, $rules);

	        if($validator->fails()) {

	            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

	        }

	        DB::beginTransaction();

	    	$poll = new Poll;
			$poll->name = $input['name'];
		    $poll->start_date = $input['start_date'];
		    $poll->end_date = $input['end_date'];
		    $poll->save();

	        foreach($input['questions'] as $q) {

		    	$question = new Question;
		    	$question->description = $q['description'];
		    	$question->type = $q['type'];
		    	$question->id_polls = $poll->id_polls;
		    	$question->save();

		    	foreach($q['options'] as $o) {
		    		$option = new Option;
		    		$option->description = $o['description'];
		    		$option->id_questions = $question->id_questions;
		    		$option->save();
		    	}
		    }
		    
		    DB::commit();

	        return Main::response(true, 'Created', $this->resolveRelations($poll), 201);

		} catch(\Exception $e) {

			DB::rollback();

			return Main::response(false, $e->getMessage(), null, 500);

		}

	}

	/**
     * update
     * Elimina la encuesta y la vuelve a crear, le asigna nuevamente las preguntas y las opciones de cada una.
     *
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Poll
     * @\App\Question
     * @\App\Option
     * 
     * @param  int      $id ID de la encuesta
     * @return response     NULL|Bad Request|Not Found|Error
     */
	public function update($id) {

		$poll = Poll::find($id);

        if($poll) {

        	try {

				$input = Request::all();

		    	$rules = [
	                'name'           => 'required|string',
	                'start_date'     => 'required|date',
	                'end_date'       => 'required|date',
	                'questions'      => 'required|array'
	            ];

	            if(isset($input['questions']) &&
	            		 $input['questions']) {

			        foreach(range(0, count($input['questions']) - 1) as $i) {

		                $rules["questions.$i.description"]  = 'required|string';
	                	$rules["questions.$i.type"]  		= 'required|string|in:single,multiple';
		                $rules["questions.$i.options"] 		= 'required|array';

		                if(isset($input['questions'][$i]['options']) && 
		                		 $input['questions'][$i]['options']) {

			                foreach(range(0, count($input['questions'][$i]['options']) - 1) as $k) {

				                $rules["questions.$i.options.$k.description"] = 'required|string';

				            }

		                }

		            }

	            }
	            	
	            $validator = Validator::make($input, $rules);

		        if($validator->fails()) {

		            return Main::response(false, 'Bad Request', ['errors' => $validator->errors()], 400);

		        }

		        DB::beginTransaction();

		        $id_polls = $poll->id_polls;
		        $poll->delete();

		    	$poll = new Poll;
		    	$poll->id_polls = $id_polls;
				$poll->name = $input['name'];
			    $poll->start_date = $input['start_date'];
			    $poll->end_date = $input['end_date'];
			    $poll->save();

		        foreach($input['questions'] as $q) {
			    	
			    	$question = new Question;
			    	$question->description = $q['description'];
		    		$question->type = $q['type'];
			    	$question->id_polls = $poll->id_polls;
			    	$question->save();

			    	foreach($q['options'] as $o) {
			    		$option = new Option;
			    		$option->description = $o['description'];
			    		$option->id_questions = $question->id_questions;
			    		$option->save();
			    	}
			    }

			    DB::commit();

		        return Main::response(true, 'OK', $this->resolveRelations($poll));

			} catch(\Exception $e) {

				DB::rollback();

				return Main::response(false, $e->getMessage(), null, 500);

			}

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

	}

	/**
     * destroy
     * Borra de la base de datos una encuesta(\App\Poll) por medio de un ID
     * 
     * @Illuminate\Foundation\Http\FormRequest
     * @\App\Poll
     * 
     * @param  int      $id ID de la encuesta
     * @return response     Ok|Not Found
     */
	public function destroy($id) {

        $poll = Poll::find($id);
        
        if ($poll) {
            
            $poll->delete();

            return Main::response(true, 'OK', null);

        } else {

            return Main::response(false, 'Not Found', null, 404);

        }

    }

    /**
     * missingPolls
     * Devuelve todas las encuestas que no tengan ninguna pregunta asignada.
     * 
     * @param int 		$id_clients ID de cliente
     * @return response             OK|Error
     */
	public function missingPolls($id_clients) {

		try {

			$polls = DB::table('polls')
				->select('*')
				->whereNotIn('id_polls', function($query) use ($id_clients) {
					$query
						->select('id_polls')
						->from('answers')
						->where('id_clients', $id_clients)
						->groupBy('id_polls');
				})->get();

			return Main::response(true, 'OK', $polls);

		} catch(\Exception $e) {

			return Main::response(false, null, $e->getMessage(), 500);

		}

	}

	/**
     * resolveRelations
     * Devuelve todos las preguntas que pertenescan a una encuesta(\App\Poll)
     * 
     * @param  Poll $poll Objeto Eloquent Poll
     * @return Poll       Objeto Eloquent Poll
     */
	private function resolveRelations(Poll $poll) {

		$questions = Question::where('id_polls', '=', $poll->id_polls)->get();

		foreach($questions as &$question)
			$question->options = Option::where('id_questions', '=', $question->id_questions)->get();

		$poll->questions = $questions;
		return $poll;

	}

}

