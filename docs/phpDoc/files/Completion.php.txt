<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Completion extends Model {

    protected $table = 'completions';
    protected $primaryKey = 'id_completions';

}

